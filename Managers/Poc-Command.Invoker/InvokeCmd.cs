﻿// INVOKER, get the choosen Command & Execute it 
using Poc_Command.Commands;
namespace Poc_Command.Invoker;

public class InvokeCmd
{
    ICommand? command;
    ICommand[] commands;
    public InvokeCmd(ICommand[] _commands)
    {
        commands = _commands;
    }

    public void GradingCalcTotal()
    {
        command = commands.Where(cmd => cmd.Name == CstCommands.GradingTotalCalculation).FirstOrDefault();
    }
    public void ProductSetupElectiveLinks()
    {
        command = commands.Where(cmd => cmd.Name == CstCommands.ProductSetupCreateElectiveLinks).FirstOrDefault();
    }
    public void ExecuteCommand()
    {
        if (command == null)
        {
            throw new Exception("Command not set");
        }
        command.Execute();
    }
}
