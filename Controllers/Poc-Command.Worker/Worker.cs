using Azure.Identity;
using Azure.Messaging.ServiceBus;
using System.Text.Json;

namespace Poc_Command.Worker;
public class Worker : BackgroundService
{
    private readonly ILogger<Worker> logger;

    // the client that owns the instance and can be used to create senders and receivers
    readonly ServiceBusClient client;

    // the processor that reads and processes messages from the queue
    readonly ServiceBusProcessor processor;
    readonly int waitingTime;

    public Worker(ILogger<Worker> _logger, IConfiguration config)
    {
        logger = _logger;
        string? serviceBusConnection = config.GetValue<string>("Servicebus:Connection");
        if (string.IsNullOrEmpty(serviceBusConnection)) { throw new Exception("No servicebus instance info"); }

        string? serviceQueue = config.GetValue<string>("Servicebus:Queue");
        if (string.IsNullOrEmpty(serviceQueue)) { throw new Exception("No servicebus queue info"); }

        waitingTime = config.GetValue<int>("Servicebus:WaitingTime");

        ServiceBusClientOptions clientOptions = new()
        {
            TransportType = ServiceBusTransportType.AmqpWebSockets
        };
        client = new ServiceBusClient(
           serviceBusConnection,
            new DefaultAzureCredential(),
            clientOptions);

        processor = client.CreateProcessor(serviceQueue, new ServiceBusProcessorOptions());
        logger.LogInformation("Processor created");
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        // add handler to process messages
        processor.ProcessMessageAsync += MessageHandler;

        // add handler to process any errors
        processor.ProcessErrorAsync += ErrorHandler;

        // start processing 
        await processor.StartProcessingAsync();
        logger.LogInformation("Processor started.");
        while (!stoppingToken.IsCancellationRequested)
        {

            await Task.Delay(30000, stoppingToken);

        }
        // stop processor is worker service is cancelled
        await processor.StopProcessingAsync();
        logger.LogInformation("Processor stoped.");
    }

    // handle received messages
    async Task MessageHandler(ProcessMessageEventArgs args)
    {
        string body = args.Message.Body.ToString();
        logger.LogDebug($"Recieved: {body}");

        dynamic? jsonObj = JsonSerializer.Deserialize<dynamic>(body);


        // complete the message. message is deleted from the queue. 
        await args.CompleteMessageAsync(args.Message);
        logger.LogDebug("Done message processing");
    }

    // handle any errors when receiving messages
    Task ErrorHandler(ProcessErrorEventArgs args)
    {
        logger.LogError(args.Exception.ToString());
        return Task.CompletedTask;
    }

}

