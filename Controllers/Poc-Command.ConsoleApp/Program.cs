﻿// CLIENT
using Poc_Command.Commands;
using Poc_Command.Commands.Grading;
using Poc_Command.Commands.ProductSetup;
using Poc_Command.Invoker;
try
{
    // set dummy inputs
    Guid recordGuid = Guid.NewGuid();
    string tableName = "Dummy";
    string dataverseInstance = "dataverse instance (dev/tst/uat/prod)";
    CommandInputs inputParams = new(recordGuid, tableName, dataverseInstance);

    // Instantiate  all Commands
    ICommand GradingCalcTotalCmd = new CalcTotalCmd(inputParams);
    ICommand ProductSetupCreateElectiveLinksCmd = new CreateElectiveLinks(inputParams);
    ICommand[] commands = [GradingCalcTotalCmd, ProductSetupCreateElectiveLinksCmd];

    // create Invoker with all Commands
    InvokeCmd invoker = new(commands);

    // choose command to execute
    Console.WriteLine("Choose Command");
    Console.WriteLine("A. Grading - Calculate total grade. ");
    Console.WriteLine("B. Product Setup - Link new elective to all product editions.");

    ConsoleKey choose = Console.ReadKey().Key;
    Console.WriteLine();

    switch (choose)
    {
        case ConsoleKey.A:
            invoker.GradingCalcTotal();
            break;
        case ConsoleKey.B:
            invoker.ProductSetupElectiveLinks();
            break;
        default:
            throw new Exception("Unknown command");
    }
    invoker.ExecuteCommand();
}
catch (Exception ex) { Console.WriteLine("ERROR " + ex.Message); }
