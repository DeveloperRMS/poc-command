namespace Poc_Command.Commands;

public interface ICommandInputs
{
    public Guid RecordGuid { get; set; }
    public string TableName { get; set; }
    public string dataverseInstance { get; set; }
}