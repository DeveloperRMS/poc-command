using Poc_Command.Commands;

public class CommandInputs(Guid recordGuid, string tableName, string dataverseInstance) : ICommandInputs
{
    public Guid RecordGuid { get; set; } = recordGuid;
    public string TableName { get; set; } = tableName;
    public string dataverseInstance { get; set; } = dataverseInstance;
}

