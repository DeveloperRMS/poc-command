﻿namespace Poc_Command.Commands;

public interface ICommand
{
    public ICommandInputs Inputs { get; set; }
    public string Name { get; }
    public void Execute();
    public void Unexecute();
}
