using Poc_Command.Receivers;

namespace Poc_Command.Commands.Grading;

public class CalcTotalCmd(ICommandInputs inputs) : ICommand
{
    public ICommandInputs Inputs { get; set; } = inputs;
    DataverseReceiver dataverse = new DataverseReceiver(inputs.dataverseInstance);

    public string Name => CstCommands.GradingTotalCalculation;

    public void Execute()
    {
        // Get all final grade from dataverse
        // Do total calculation
        // Write result to dataverse
        Console.WriteLine($"Do final grade calculation for {Inputs.RecordGuid} from table {Inputs.TableName}");
    }

    public void Unexecute()
    {
        throw new NotImplementedException();
    }
}