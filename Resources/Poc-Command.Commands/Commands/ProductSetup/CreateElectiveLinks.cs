using Poc_Command.Receivers;

namespace Poc_Command.Commands.ProductSetup;

public class CreateElectiveLinks(ICommandInputs inputs) : ICommand
{
    public string Name => CstCommands.ProductSetupCreateElectiveLinks;
    public ICommandInputs Inputs { get; set; } = inputs;
    DataverseReceiver dataverse = new DataverseReceiver(inputs.dataverseInstance);

    public void Execute()
    {
        // Write result to dataverse
        Console.WriteLine($"Elective {Inputs.RecordGuid} is linked");
    }

    public void Unexecute()
    {
        throw new NotImplementedException();
    }
}