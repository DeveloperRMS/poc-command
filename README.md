# Example Command pattern

## Clients
**Controllers\Poc-Command.ConsoleApp**
Instantiate all Commands, used the Invoker to choose and execute the Command
- Console app
- Worker

## Invoker 
**Managers\Poc-Command.Invoker**
Set the requested Command, makes it executable for the Client

## Commands
**Resources\Poc-Command.Commands**
_Interfaces_
* ICommand: ICommandInputs common object property, Execute & Unexecuted methods
* ICommandInputs= common properties for all commands
_Commands_
Use receiver for CRUD operations

## Receivers 
** Resources\Poc-Command.Receivers**
Dataverse receiver
